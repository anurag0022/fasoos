package in.picbooth.faasos;

/**
 * Created by AnuraG on 26-09-2015.
 */
public class FoodItem {
    private String name;
    private String image;
    private String category;
    private int spice_meter;
    private String description;
    private float rating;
    private int price;
    private String is_veg;

    public FoodItem() {

    }

    public FoodItem(String name, String image, String category, int spice_meter, String description, float rating, int price, String is_veg) {
        this.name = name;
        this.image = image;
        this.category = category;
        this.spice_meter = spice_meter;
        this.description = description;
        this.rating = rating;
        this.price = price;
        this.is_veg = is_veg;
    }

    public String getName() {
        return name;
    }

    public String getImage() {
        return image;
    }

    public String getCategory() {
        return category;
    }

    public float getRating() {
        return rating;
    }

    public int getPrice() {
        return price;
    }

    public int getSpice_meter() {
        return spice_meter;
    }

    public String getDescription() {
        return description;
    }

    public String getIs_veg() {
        return is_veg;
    }


}