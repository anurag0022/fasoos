package in.picbooth.faasos;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.Image;
import android.media.Rating;
import android.os.AsyncTask;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.telephony.SmsManager;
import android.text.InputType;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.io.InputStream;
import java.net.URL;
import java.util.concurrent.ExecutionException;

public class activity_showitem extends Activity {
    Bitmap backImage;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_activity_showitem);
        final TextView name = (TextView) findViewById(R.id.name);
        TextView price = (TextView) findViewById(R.id.price);
        RatingBar rate = (RatingBar) findViewById(R.id.rating);
        TextView desc = (TextView) findViewById(R.id.desc);
        TextView spice = (TextView) findViewById(R.id.spice);
        TextView cat = (TextView) findViewById(R.id.cat);
        TextView veg = (TextView) findViewById(R.id.veg);
        ImageView image = (ImageView) findViewById(R.id.image);

        TextView like = (TextView) findViewById(R.id.like);
        TextView share = (TextView) findViewById(R.id.share);
        TextView back = (TextView) findViewById(R.id.back);
        TextView sms = (TextView) findViewById(R.id.sms);

        Bundle extras = getIntent().getExtras();
        name.setText(extras.getString("name"));
        price.setText(String.valueOf("Rs " + extras.getInt("price") + "/-"));
        rate.setRating(extras.getFloat("rating"));
        desc.setText(extras.getString("description"));
        cat.setText(extras.getString("category"));
        if (extras.getFloat("spice_meter") != 0f)
            spice.setText(String.valueOf(extras.getFloat("spice_meter")));
        else
            spice.setText("Not spicy, At all");
        veg.setText(extras.getString("is_veg").toUpperCase());
        new DownloadImageTask((ImageView) findViewById(R.id.image))
                .execute(extras.getString("image"));
        image.setScaleType(ImageView.ScaleType.FIT_XY);
        like.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Coming Soon!", Toast.LENGTH_SHORT).show();
            }
        });
        share.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mShareIntent;
                mShareIntent = new Intent();
                mShareIntent.setAction(Intent.ACTION_SEND);
                mShareIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                mShareIntent.setType("text/plain");
                mShareIntent.putExtra(Intent.EXTRA_TEXT, "Found the yummiest " + name.getText() + " on FAASOS app! Made by anurag.");
                getApplicationContext().startActivity(mShareIntent);
            }
        });
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        sms.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                final EditText input = new EditText(getApplicationContext());
                input.setInputType(InputType.TYPE_CLASS_NUMBER);
                input.setTextColor(Color.BLACK);
                new AlertDialog.Builder(activity_showitem.this)
                        .setView(input)
                        .setTitle("Enter phone number:")
                        .setPositiveButton("Send", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                SmsManager sms = SmsManager.getDefault();
                                sms.sendTextMessage(input.toString(), null, "Found the yummiest " + name.getText() + " on FAASOS app! Made by anurag.", null, null);
                            }
                        })
                        .setNegativeButton("Later", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                            }
                        })
                        .show();

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_showitem, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}
