package in.picbooth.faasos;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.os.AsyncTask;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;


import com.android.volley.VolleyError;
import com.android.volley.toolbox.ImageLoader;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

public class FoodAdapter extends ArrayAdapter<FoodItem> {
    Context context;

    public FoodAdapter(Context context, int resource, ArrayList<FoodItem> foodMenu) {
        super(context, resource, foodMenu);
        this.context = context;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        FoodItem foodItem = getItem(position);
        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.layout_listitem, parent, false);
        }
        TextView nameView = (TextView) convertView.findViewById(R.id.itemName);
        TextView priceView = (TextView) convertView.findViewById(R.id.itemPrice);
        RatingBar ratingView = (RatingBar) convertView.findViewById(R.id.itemRate);
        final ImageView imageView = (ImageView) convertView.findViewById(R.id.itemImg);
//        new DownloadImageTask((ImageView) convertView.findViewById(R.id.itemImg))
//               .execute(foodItem.getImage());

        nameView.setText(foodItem.getName());
        priceView.setText(String.valueOf("Rs " + foodItem.getPrice() + "/-"));
        ratingView.setRating(foodItem.getRating());
        ratingView.setEnabled(false);
        ImageLoader imageLoader = AppController.getInstance().getImageLoader();
        imageLoader.get(foodItem.getImage(), new ImageLoader.ImageListener() {
            @Override
            public void onResponse(ImageLoader.ImageContainer response, boolean isImmediate) {
                imageView.setImageBitmap(response.getBitmap());
            }

            @Override
            public void onErrorResponse(VolleyError error) {
                Log.e("Image loader", "Image Load Error: " + error.getMessage());
            }
        });
        return convertView;
    }

    private class DownloadImageTask extends AsyncTask<String, Void, Bitmap> {
        ImageView bmImage;

        public DownloadImageTask(ImageView bmImage) {
            this.bmImage = bmImage;
        }

        protected Bitmap doInBackground(String... urls) {
            String urldisplay = urls[0];
            Bitmap mIcon11 = null;
            try {
                InputStream in = new URL(urldisplay).openStream();
                mIcon11 = BitmapFactory.decodeStream(in);
            } catch (Exception e) {
                Log.e("Error", e.getMessage());
                e.printStackTrace();
            }
            return mIcon11;
        }

        protected void onPostExecute(Bitmap result) {
            bmImage.setImageBitmap(result);
        }
    }
}