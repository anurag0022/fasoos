package in.picbooth.faasos;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.AppCompatSpinner;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.concurrent.ExecutionException;

public class activity_main extends Activity {
    private String menuUrl = "http://faasos.0x10.info/api/faasos?type=json&query=list_menu";
    private String apiUrl = "http://faasos.0x10.info/api/faasos?type=json&query=api_hits";
    ArrayList<FoodItem> foodMenu = new ArrayList<>();
    ArrayList<FoodItem> foodMenuVeg = new ArrayList<>();

    ArrayList<String> foodList = new ArrayList<>();
    private int apiCalls;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.layout_activity_main);

        //Declarations
        final AutoCompleteTextView searchMenu = (AutoCompleteTextView) findViewById(R.id.searchMenu);
        final SwitchCompat foodType = (SwitchCompat) findViewById(R.id.vegSwitch);
        final ListView menuList = (ListView) findViewById(R.id.menuListView);
        TextView sortByPrice = (TextView) findViewById(R.id.sortByPrice);
        TextView sortByRating = (TextView) findViewById(R.id.sortByRating);
        final TextView foodCount = (TextView) findViewById(R.id.foodCount);
        TextView apiHits = (TextView) findViewById(R.id.apiHits);
        //Adding Values
        try {
            new GetMenu().execute().get();
        } catch (InterruptedException | ExecutionException e) {
            e.printStackTrace();
        }
        //Search
        ArrayAdapter searchFood = new ArrayAdapter(getApplicationContext(), R.layout.layout_searchitem, foodList);
        searchMenu.setAdapter(searchFood);
        searchMenu.setThreshold(1);
        //List Menu
        final FoodAdapter foodAdapter = new FoodAdapter(getApplicationContext(), R.layout.layout_listitem, foodMenu);
        foodAdapter.setNotifyOnChange(true);
        menuList.setAdapter(foodAdapter);
        //Search Select

        searchMenu.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                for (int i = 0; i < foodMenu.size(); i++) {
                    if (foodMenu.get(i).getName().equals(String.valueOf(parent.getItemAtPosition(position)))) {
                        Intent next = new Intent(getApplicationContext(), activity_showitem.class);
                        next.putExtra("image", foodMenu.get(i).getImage());
                        next.putExtra("name", foodMenu.get(i).getName());
                        next.putExtra("price", foodMenu.get(i).getPrice());
                        next.putExtra("rating", foodMenu.get(i).getRating());
                        next.putExtra("spice_meter", foodMenu.get(i).getSpice_meter());
                        next.putExtra("description", foodMenu.get(i).getDescription());
                        next.putExtra("category", foodMenu.get(i).getCategory());
                        next.putExtra("is_veg", foodMenu.get(i).getIs_veg());
                        startActivity(next);
                        break;
                    }
                }

            }
        });
        searchMenu.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        //List select
        menuList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent next = new Intent(getApplicationContext(), activity_showitem.class);
                next.putExtra("image", foodMenu.get(position).getImage());
                next.putExtra("name", foodMenu.get(position).getName());
                next.putExtra("price", foodMenu.get(position).getPrice());
                next.putExtra("rating", foodMenu.get(position).getRating());
                next.putExtra("spice_meter", foodMenu.get(position).getSpice_meter());
                next.putExtra("description", foodMenu.get(position).getDescription());
                next.putExtra("category", foodMenu.get(position).getCategory());
                next.putExtra("is_veg", foodMenu.get(position).getIs_veg());
                startActivity(next);
            }
        });
        //Food Type
        foodType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (foodType.isChecked()) {
                    foodAdapter.clear();
                    foodAdapter.addAll(foodMenuVeg);
                    foodAdapter.notifyDataSetChanged();
                    foodCount.setText(String.valueOf(foodMenuVeg.size()));
                } else {
                    foodAdapter.addAll(foodMenu);
                    foodAdapter.notifyDataSetChanged();
                    foodCount.setText(String.valueOf(foodMenu.size()));
                }
            }
        });
        //Sort By
        sortByPrice.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortBy('P');
                foodAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Cheapest on the Top!", Toast.LENGTH_SHORT).show();
            }
        });
        sortByRating.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sortBy('R');
                foodAdapter.notifyDataSetChanged();
                Toast.makeText(getApplicationContext(), "Yummiest on the Top!", Toast.LENGTH_SHORT).show();
            }
        });
        //FoodCount
        if (foodType.isChecked()) {
            foodCount.setText(String.valueOf(foodMenuVeg.size()));
        } else {
            foodCount.setText(String.valueOf(foodMenu.size()));
        }
        apiHits.setText(String.valueOf(apiCalls));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private class GetMenu extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... arg0) {
            // Creating service handler class instance
            ServiceHandler sh = new ServiceHandler();
            String jsonStr = sh.makeServiceCall(getApplicationContext(), menuUrl, ServiceHandler.GET);
            FoodItem foodItem;
            if (jsonStr != null) {
                try {
                    JSONObject itemObj = new JSONObject(jsonStr);
                    JSONArray itemArr = itemObj.getJSONArray("menu");
                    for (int i = 0; i < itemArr.length(); i++) {
                        JSONObject itemObj2 = itemArr.getJSONObject(i);
                        foodItem = new FoodItem(itemObj2.getString("name"), itemObj2.getString("image"), itemObj2.getString("category"), Integer.parseInt(itemObj2.getString("spice_meter")), itemObj2.getString("description"), Float.parseFloat(itemObj2.getString("rating")), Integer.parseInt(itemObj2.getString("price")), itemObj2.getString("is_veg"));
                        foodMenu.add(foodItem);
                        if (itemObj2.getString("is_veg").equals("yes")) {
                            foodMenuVeg.add(foodItem);
                        }
                        foodList.add(itemObj2.getString("name"));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                Log.e("ServiceHandler", "Couldn't get any data from the url");
            }
            jsonStr = sh.makeServiceCall(getApplicationContext(), apiUrl, ServiceHandler.GET);
            if (jsonStr != null) {
                try {
                    JSONObject jsonObject = new JSONObject(jsonStr);
                    apiCalls = jsonObject.getInt("api_hits");
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
            // Dismiss the progress dialog
        }
    }

    void sortBy(char type) {
        if (type == 'P') {
            Collections.sort(foodMenu, new Comparator<FoodItem>() {
                @Override
                public int compare(FoodItem lhs, FoodItem rhs) {
                    return lhs.getPrice() - rhs.getPrice();
                }
            });
        } else {
            Collections.sort(foodMenu, new Comparator<FoodItem>() {
                @Override
                public int compare(FoodItem lhs, FoodItem rhs) {
                    return (int) rhs.getRating() - (int) lhs.getRating();
                }
            });
        }
    }
}
